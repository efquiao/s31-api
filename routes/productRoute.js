const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

router.get("/", (req, res) => {
  productController
    .getAllProducts()
    .then((resultFromController) => res.send(resultFromController));
});

router.post("/", (req, res) => {
  
  productController
    .createProduct(req.body)
    .then((resultFromController) => res.send(`You have added ${resultFromController.name} with a price of ${resultFromController.price}`));
});

router.get("/:id", (req, res) => {

  productController
    .getProduct(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});


router.delete("/delete/:id", (req, res) => {
  
  productController
    .deleteProduct(req.params.id)
    .then((resultFromController) => res.send(`successfully deleted ${resultFromController}`));
});

// use "module.exports" to export the router to use in the "app.js"
module.exports = router;
