const express = require('express')
const mongoose = require('mongoose') 

const productRoute = require('./routes/productRoute')

const app = express()
const port = 3500

app.use(express.json())
app.use(express.urlencoded({extended:true}))



mongoose.connect(
  "mongodb+srv://admin:admin1234@zuitt-bootcamp.vhxdp.mongodb.net/api?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use('/products', productRoute)


app.listen(port, ()=> console.log(`listening to port ${port}`))

//console.log(app)