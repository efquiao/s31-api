const Product = require("../models/product");

module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

module.exports.createProduct = (requestBody) => {
  let newProduct = new Product({
    name: requestBody.name,
    price: requestBody.price
  });
  return newProduct.save().then((product, error) => {
    if (error) {
      console.log(error);
    } else {
      return product;
    }
  });
};

module.exports.getProduct = (productId) => {
 
  return Product.findById(productId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return result;
    }
  });
};

module.exports.deleteProduct = (productId) => {
  return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return removedProduct;
    }
  });
};
